# Compose Multiplatform test

Port of [Gugal's](https://gitlab.com/narektor/gugal) about activity to [Compose Multiplatform](https://www.jetbrains.com/lp/compose-mpp/).

![Demo screenshot](demo.png)

> Let me answer this first: no, there are **no plans** to port the entire app to Compose Multiplatform. If you want to port it yourself feel free, but there will not be an official port.

## Changes from Gugal

The [original code](https://gitlab.com/narektor/gugal/-/blob/main/app/src/main/java/com/porg/gugal/ui/AboutActivity.kt) had to be changed slightly:
- The Multiplatform Material 3 libraries wouldn't work, so the UI had to be changed to be based on Material 2 instead.
- `open()` had to be changed because there are (obviously) no custom tabs on Multiplatform. Now it opens the system browser.
- The back button's `onClick` has been changed to quit the app instead.
- "What's new" opens this repository's commits page.
- `com.porg.gugal.R` doesn't exist, so:
    - an `Rstring` class has been made to replace `R.string.*` calls,
    - the syntax of `Contributor()` has been changed to fit the Multiplatform resource system.
- Any function that requires a `Context` doesn't anymore.

## Building

Just open the project in IntelliJ IDEA.

To build it, use the `run` task under `compose multiplatform` in the Gradle tasks list, or `gradlew run` in the terminal.
