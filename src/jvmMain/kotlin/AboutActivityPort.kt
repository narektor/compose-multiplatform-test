/*
 *     Based on Gugal's AboutActivity.kt. The original copyright notice continues.
 *
 *     AboutActivity.kt
 *     Gugal
 *     Copyright (c) 2023 thegreatporg
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import java.awt.Desktop
import java.net.URI
import java.util.*


class AboutActivity {

    @ExperimentalAnimationApi
    @Composable
    fun onCreate(back: (() -> Unit)) {
        Surface(color = MaterialTheme.colors.surface) {
            Column(
                modifier = Modifier.verticalScroll(rememberScrollState())
            ) {
                IconButton(
                    onClick = back,
                    modifier = Modifier.padding(
                        start = 16.dp,
                        top = 16.dp,
                        bottom = 0.dp,
                        end = 16.dp
                    )
                ) {
                    Icon(
                        imageVector = Icons.Filled.ArrowBack,
                        contentDescription = "Go back",
                    )
                }
                Column(modifier = Modifier.fillMaxSize()) {
                    // Gugal logo
                    Card(
                        modifier = Modifier
                            .padding(all = 24.dp)
                            .size(128.dp)
                            .align(alignment = Alignment.CenterHorizontally),
                        shape = CircleShape,
                        backgroundColor = Color(0x00, 0x99, 0xff)
                    ) {
                        Image(
                            painterResource("search_glass.png"),
                            contentDescription = "Logo",
                            contentScale = ContentScale.Crop,
                            modifier = Modifier.fillMaxSize()
                        )
                    }
                    Text(
                        text = "Gugal " + getVersion(),
                        modifier = Modifier.fillMaxWidth(),
                        textAlign = TextAlign.Center,
                        fontSize = MaterialTheme.typography.h6.fontSize
                    )
                }
                Material3Settings.RegularSetting(
                    Rstring.setting_donate_title,
                    Rstring.setting_donate_desc,
                    onClick = { open("https://ko-fi.com/thegreatporg") }
                )
                Material3Settings.RegularSetting(
                    Rstring.setting_issue_title,
                    Rstring.setting_issue_desc,
                    onClick = {
                        open(
                            "https://gitlab.com/narektor/gugal/-/issues/new"
                        )
                    }
                )
                Material3Settings.RegularSetting(
                    Rstring.setting_gitlab_title,
                    Rstring.setting_gitlab_desc,
                    onClick = { open("https://gitlab.com/narektor/gugal") }
                )
                Material3Settings.RegularSetting(
                    Rstring.setting_changelog_title,
                    Rstring.setting_changelog_desc,
                    onClick = {
                        // Replaced with opening the commits page for this project.
                        open("https://gitlab.com/narektor/compose-multiplatform-test/-/commits/main")
                    }
                )
                Material3Settings.RegularSetting(
                    Rstring.setting_translate_title,
                    Rstring.setting_translate_desc,
                    onClick = { open("https://hosted.weblate.org/engage/gugal/") }
                )
                Divider(
                    modifier = Modifier
                        .fillMaxWidth()
                        .width(1.dp)
                        .padding(start = 16.dp, end = 16.dp)
                )
                Text(
                    text = Rstring.about_contributors_start,
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(top = 24.dp),
                    textAlign = TextAlign.Center,
                    style = MaterialTheme.typography.body1
                )

                Contributor(
                    "narektor",
                    Rstring.role_developer,
                    "https://gitlab.com/narektor",
                    "narektor"
                )
                Contributor(
                    "iSteven",
                    Rstring.role_designer,
                    "https://t.me/OneSteven",
                    "steven"
                )
                ContributorForLocale(Locale.ENGLISH)
            }
        }
    }

    @Composable
    fun ContributorForLocale(locale: Locale) {
        // Find the language code at https://www.iana.org/assignments/language-subtag-registry/language-subtag-registry,
        // it's usually written after "Subtag:"
        when (locale.language) {
            // Languages with >=75% translated
            "ko" -> {
                Contributor(
                    "Junghee",
                    "translator",
                    "https://gitlab.com/Junghee_Lee",
                    "junghee"
                )
            }
            "tr" -> {
                Contributor(
                    "metezd",
                    "translator",
                    "mailto:itoldyouthat@protonmail.com",
                    "metezd"
                )
                Contributor(
                    "Oğuz Ersen",
                    "translator",
                    "https://ersen.moe/",
                    "ersen"
                )
            }
            "in" -> {
                Contributor(
                    "Linerly",
                    "translator",
                    "https://linerly.tk",
                    "linerly"
                )
            }
            // Languages with >=15% translated
            "nb" -> {
                Contributor(
                    "Allan Nordhøy",
                    "translator",
                    "https://gitlab.com/kingu",
                    "kingu"
                )
            }
            "fr" -> {
                Contributor(
                    "J. Lavoie",
                    "translator",
                    "mailto:j.lavoie@net-c.ca",
                    "lavoie"
                )
            }
            // Please don't add contributors for languages with less than 15% translated.
        }

        // Special case for Simplified Chinese (zh-Hans)
        if (locale.language == "zh" && locale.script == "Hans") {
            Contributor(
                "Hugel",
                "translator",
                "mailto:qihu@nfschina.com",
                "hugel"
            )
        }
    }

    // A Material3Settings-like component displaying information about a contributor.
    @Composable
    fun Contributor(name: String, role: String, url: String, filename: String) {
        Row(modifier = Modifier
            .clickable { open(url) }
            .fillMaxWidth()
            .padding(start = Material3Settings.padding),
            verticalAlignment = Alignment.CenterVertically) {
            Image(
                painter = painterResource("cont_${filename}.png"),
                contentDescription = "",
                contentScale = ContentScale.Crop,
                modifier = Modifier
                    .size(48.dp)
                    .clip(CircleShape)
            )
            Column(modifier = Modifier.padding(start = 15.dp)) {
                Text(
                    text = name,
                    modifier = Modifier.padding(end = Material3Settings.padding, top = Material3Settings.padding, bottom = Material3Settings.dividerPadding),
                    fontSize = MaterialTheme.typography.h6.fontSize
                )
                Text(
                    text = role,
                    modifier = Modifier.padding(end = Material3Settings.padding, bottom = Material3Settings.padding),
                    style = MaterialTheme.typography.body1
                )
            }
        }
    }

    private fun open(url: String) {
        val desktop = if (Desktop.isDesktopSupported()) Desktop.getDesktop() else null
        if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
            try {
                desktop.browse(URI.create(url))
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun getVersion(): String {
        return "multiplat-test"
    }
}