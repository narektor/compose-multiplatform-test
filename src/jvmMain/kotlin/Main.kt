import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application

@OptIn(ExperimentalAnimationApi::class)
fun main() = application {
    var isOpen by remember { mutableStateOf(true) }
    if (isOpen) {
        Window(onCloseRequest = ::exitApplication, title = Rstring.app_name) {
            val aboutActivity = AboutActivity()
            MaterialTheme {
                aboutActivity.onCreate { isOpen = false }
            }
        }
    }
}
